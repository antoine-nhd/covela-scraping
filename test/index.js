process.env.NODE_ENV = 'test'

const Character = require('../models/characters')
const Update = require('../models/updates')
const chai = require('chai')
const { expect } = chai
const chaiHttp = require('chai-http')
const nock = require('nock')
const server = require('../index')
chai.use(chaiHttp)

const dropDb = async () => {
  await Character.remove({})
  await Update.remove({})
}

describe('endpoints', () => {
  beforeEach(async (done) => {
    await dropDb()
    done()
  })

  describe('/GET characters', () => {
    it('should return all the characters', async () => {
      const naruto =
        await Character.create(
          {
            name: 'naruto',
            picture: 'http://naruto.com',
            textContent: [{ tag: 'p', content: 'rasengan' }]
          }
        )
      const response = await chai.request(server).get('/characters')
      expect(response.status).to.equal(200)
      expect(response.body).to.be.an('Array')
      expect(response.body[0].name).to.equal(naruto.name)
      expect(response.body[0].picture).to.equal(naruto.picture)
      expect(response.body[0].textContent).to.equal(undefined)
    })
  })

  describe('/GET characters/:id', () => {
    it('should return all the character', async () => {
      const naruto =
        await Character.create(
          {
            name: 'naruto',
            picture: 'http://naruto.com',
            textContent: [{ tag: 'p', content: 'rasengan' }]
          }
        )

      const response = await chai.request(server).get(`/characters/${naruto._id}`)
      expect(response.status).to.equal(200)
      expect(response.body.name).to.equal(naruto.name)
      expect(response.body.picture).to.equal(naruto.picture)
      expect(response.body.textContent[0].tag).equal('p')
    })
  })

  describe('/GET last-update', () => {
    it('should return the last update', async () => {
      await Update.create({ success: true })
      const lastUpdate =
        await Update.create(
          {
            success: true
          }
        )
      const response = await chai.request(server).get('/last-update')
      expect(response.status).to.equal(200)
      expect(response.body.success).to.equal(lastUpdate.success)
      expect(response.body.created_at).to.equal(lastUpdate.created_at)
    })
  })

  describe('/GET scrape', () => {
    it('should run the scraper', async (done) => {
      const response = await chai.request(server).get('/scrape')
      expect(response.status).to.equal(200)
      expect(response.body.scrappingHasStarted).to.equal(true)
      setTimeout(async () => {
        const characters = await Character.find()
        const updates = await Update.find()
        expect(characters[0].picture).to.equal('https://vignette.wikia.nocookie.net/naruto/images/b/bd/Boruto_uzumaki.png/revision/latest/scale-to-width-down/300?cb=20150416123755')
        expect(characters[0].name).to.equal('Boruto Uzumaki')
        expect(updates[0].success).to.be.true
        expect(updates[0].created_at).to.be.above(new Date() - 100000)
        done()
      }, 50000)
    })
    describe('when there is an error', () => {
      it('should exectue error callback', async (done) => {
        nock('http://naruto.wikia.com/wiki/Boruto_Uzumaki')
          .get()
          .reply(500)
        const response = await chai.request(server).get('/scrape')
        expect(response.status).to.equal(200)
        expect(response.body.scrappingHasStarted).to.equal(true)
        setTimeout(async () => {
          const updates = await Update.find()
          expect(updates[0].success).to.be.false
          expect(updates[0].created_at).to.be.above(new Date() - 10000)
          done()
        }, 3000)
        nock.cleanAll()
      })
    })
  })
})
