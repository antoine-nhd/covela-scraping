const getCharactersUri = require('../../helpers/getCharactersUri')
const { expect } = require('chai')

describe('getCharactersUri', () => {
  it('should get all the characters uri', async () => {
    const uris = await getCharactersUri(1)
    expect(uris.length).to.equal(16)
    expect(uris[0]).to.equal('http://naruto.wikia.com/wiki/Boruto_Uzumaki')
  })
})
