const getCharacterData = require('../../helpers/getCharacterData')
const { expect } = require('chai')

describe('getCharacterData', () => {
  it('should get data for a character', async () => {
    const character = await getCharacterData('http://naruto.wikia.com/wiki/Boruto_Uzumaki')
    expect(character.picture).to.equal('https://vignette.wikia.nocookie.net/naruto/images/b/bd/Boruto_uzumaki.png/revision/latest/scale-to-width-down/300?cb=20150416123755')
    expect(character.name).to.equal('Boruto Uzumaki')
  })
})
