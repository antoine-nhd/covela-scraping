const getNumberOfCharatersPages = require('../../helpers/getNumberOfCharatersPages')
const { expect } = require('chai')

describe('getNumberOfCharatersPages', () => {
  it('should get the number of characters pages', async () => {
    const numberOfPages = await getNumberOfCharatersPages()
    expect(numberOfPages).to.equal(76)
  })
})
