const mongoose = require('mongoose')
const config = require('./config/config.json')
const mongoDB = process.env.DB_URI || config[process.env.NODE_ENV].dbUri
mongoose.connect(mongoDB)
mongoose.Promise = global.Promise
const db = mongoose.connection
db.on('error', console.error.bind(console, 'MongoDB connection error:'))
