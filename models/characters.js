var mongoose = require('mongoose')
var characterSchema = new mongoose.Schema({
  name: String,
  picture: String,
  textContent: [{ tag: String, content: String }]
})
module.exports = mongoose.model('Character', characterSchema)
