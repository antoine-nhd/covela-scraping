var mongoose = require('mongoose')
var updateSchema = new mongoose.Schema({
  success: Boolean,
  created_at: Date
})
module.exports = mongoose.model('Update', updateSchema)
