const request = require('request-promise')
const cheerio = require('cheerio')

module.exports = async (numberOfCharactersPages) => {
  const listOfCharactersPages = []
  for (let i = 1; i < numberOfCharactersPages + 1; i++) {
    const CharacterPageUri =
      `http://naruto.wikia.com/wiki/Category:Characters?page=${i}`
    const characterPageResponse = await request(CharacterPageUri)
    const $characterPage = cheerio.load(characterPageResponse)
    const characterUrls = $characterPage('#mw-pages .category-gallery-item a')
    for (let j = 0; j < characterUrls.length; j++) {
      listOfCharactersPages.push(characterUrls[j].attribs['href'])
    }
  }
  return listOfCharactersPages
}
