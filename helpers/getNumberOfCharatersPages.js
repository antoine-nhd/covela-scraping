const request = require('request-promise')
const cheerio = require('cheerio')

module.exports = async () => {
  const firstCharacterPageResponse =
    await request('http://naruto.wikia.com/wiki/Category:Characters')
  const $firstCharacterPage = cheerio.load(firstCharacterPageResponse)
  const numberOfCharactersPages =
    parseInt(
      $firstCharacterPage('#mw-pages ul li:nth-last-child(2) a')[0]
        .attribs['data-page']
    )
  return numberOfCharactersPages
}
