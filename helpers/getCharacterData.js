const request = require('request-promise')
const cheerio = require('cheerio')

module.exports = async (characterUri) => {
  const characterPageResponse = await request(characterUri)
  const $characterPage = cheerio.load(characterPageResponse)
  const characterPicture = $characterPage('.imagecell img')[0] ? $characterPage('.imagecell img')[0].attribs['data-src'] : null
  const rawData = $characterPage('#mw-content-text').children().filter((i, element) => {
    return (element.name === 'p' || element.name === 'h2' || element.name === 'ul')
  })
  const textContent = []
  for (let i = 0; i < rawData.length; i++) {
    textContent.push({
      tag: rawData.eq(i)['0'].name,
      content: rawData.eq(i).text()
    })
  }
  const characterData = {
    name: $characterPage('.page-header__title')[0].children[0].data,
    picture: characterPicture,
    textContent
  }
  return characterData
}
