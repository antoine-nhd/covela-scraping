require('./mongodb')
const config = require('./config/config.json')
const express = require('express')
const cors = require('cors')
const Character = require('./models/characters')
const Update = require('./models/updates')
const bodyparser = require('body-parser')
const getCharactersUri = require('./helpers/getCharactersUri')
const getCharacterData = require('./helpers/getCharacterData')
const getNumberOfCharacterPages =
  config[process.env.NODE_ENV].getNumberOfCharatersPagesPath
    ? require(config[process.env.NODE_ENV].getNumberOfCharatersPagesPath)
    : require('./helpers/getNumberOfCharatersPages')
const app = express()
const PORT = process.env.PORT || 5000

app.use(bodyparser.urlencoded({ extended: true }))
app.use(bodyparser.json())
app.use(cors())

const server = app.listen(PORT, () => {
  console.log(`listening on ${PORT}`)
})

const errorCallback = async (err, message) => {
  console.log(`${message} `, err)
  await Update.create({ created_at: new Date(), success: false })
}

app.get('/scrape', async (req, res) => {
  res.status(200)
  res.send({ scrappingHasStarted: true })
  let numberOfCharactersPages
  let listOfCharactersPages
  try {
    numberOfCharactersPages =
      await getNumberOfCharacterPages()
  } catch (err) {
    return errorCallback('error when getting number of characters :', err)
  }
  try {
    listOfCharactersPages =
      await getCharactersUri(numberOfCharactersPages)
  } catch (err) {
    return errorCallback('error when getting characters uri :', err)
  }
  for (let i = 0; i < listOfCharactersPages.length; i++) {
    let character
    try {
      character = await getCharacterData(listOfCharactersPages[i])
    } catch (err) {
    }
    try {
      await Character.findOneAndUpdate(
        { name: character.name },
        character,
        { upsert: true })
    } catch (err) {
      return errorCallback('error when writing in db :', err)
    }
  }
  await Update.create({ created_at: new Date(), success: true })
  console.log('job success')
})

app.get('/characters', async (req, res) => {
  try {
    const characters = await Character.find({}, { textContent: 0 })
    return res.send(characters)
  } catch (err) {
    return res.status(500)
  }
})

app.get('/last-update', async (req, res) => {
  try {
    const lastUpdate = await Update.findOne().sort({ field: 'asc', _id: -1 })
    return res.send(lastUpdate)
  } catch (err) {
    return res.status(500)
  }
})

app.get('/characters/:id', async (req, res) => {
  try {
    const character = await Character.findById(req.params.id)
    return res.send(character)
  } catch (err) {
    return res.status(500)
  }
})

module.exports = server
